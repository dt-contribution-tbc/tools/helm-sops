# hadolint ignore=DL3007
FROM registry.hub.docker.com/alpine/helm:latest

ADD https://github.com/getsops/sops/releases/download/v3.8.1/sops-v3.8.1.linux.amd64 /usr/local/bin/sops
RUN chmod +x /usr/local/bin/sops

WORKDIR /apps
ENTRYPOINT ["helm"]